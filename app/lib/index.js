const commandExists = require('command-exists').sync
const execFile = require('child_process').execFile
// const FFMPEG = require('ffmpeg-static')
const isThere = require('is-there')

const outputFilename = (file, part) => {
  /** split returns array */
  const parts = file.split('.')

  /** pop removes extension from parts and returns it */
  const extension = parts.pop()

  /** extend filename with .new or part_${part} */
  const add = part ? `deel_${part}` : 'nieuw'
  const result = parts.concat(add, extension)
  /** join array to one string with dots between parts */
  return result.join('.').toLowerCase()
}

const stringTime2seconds = time => {
  const hms = time.split(':')

  switch (hms.length) {
    case 2:
      // consider input as m:s
      return hms[0] * 60 + +hms[1]
    case 3:
      // consider input as h:m:s
      return hms[0] * 3600 + hms[1] * 60 + +hms[2]
  }
}
/**
 * Trim a mp3-file
 * @param {string} file filename
 * @param {string} start m:s / h:m:s
 * @param {string} end m:s / h:m:s
 */
const y3trim = (options) => {
  return new Promise((resolve, reject) => {
    console.log(options.file, options.start, options.end)

    if (!commandExists('ffmpeg')) return reject(new Error('ffmpeg not found.'))

    if (!isThere(options.file)) return reject(new Error(`File ${options.file} not found.`))

    const startSecond = stringTime2seconds(options.start)
    let endSecond = options.end ? stringTime2seconds(options.end) : undefined
    let lengthInSeconds = endSecond ? endSecond - startSecond : 80 * 60
    const minutes = Math.floor(lengthInSeconds / 60)
    const seconds = lengthInSeconds - minutes * 60
    const length = minutes + ':' + seconds
    const fragments = Math.ceil((lengthInSeconds / 60) / 74)
    // console.log('start second', startSecond)
    // console.log('end second', endSecond)
    // console.log('duration (s)', lengthInSeconds)
    // console.log('duration (m:s)', length)
    const promises = []
    // let out = outputFilename(file)

    let args = ['-i', options.file, '-af', 'atrim=' + startSecond + ':' + endSecond, outputFilename(options.file), '-y']
    let args2

    /**
     * Split output in two files
     */
    if (options.split) {
      const stop = stringTime2seconds(options.split)
      args = ['-i', options.file, '-af', 'atrim=' + startSecond + ':' + stop, outputFilename(options.file, 1), '-y']

      let start = stop
      args2 = ['-i', options.file, '-af', 'atrim=' + start + ':' + endSecond, outputFilename(options.file, 2), '-y']
    }

    promises.push(ffmpeg(args))
    if (args2) promises.push(ffmpeg(args2))

    Promise.all(promises).then(() => resolve()).catch(err => reject(err))
  })
}

const ffmpeg = args => {
  return new Promise((resolve, reject) => {
    execFile('ffmpeg', args, (error, stdout, stderr) => {
      if (error) {
        console.error(error)
        return reject(error)
      }
      console.log(stdout)
      console.log(`${args[4]} finished`)
      resolve()
    })
  })
}

module.exports = y3trim
