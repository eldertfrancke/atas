/**
 * Module for removing a part of an mp3-file.
 * First we split the file in two parts.
 * Next we concanate them together.
 *
 * The commands we can use are:
 * - ffmpeg -i "concat:file-part1.mp3|file-part2.mp3" -c copy result.mp3
 * But maybe also this more advanced one:
 * - ffmpeg -i INPUT -af atrim=60:120 (start:end) (drop everything except second minute of input)
 */

const execFile = require('child_process').execFile

const cut = (file) => {
  return new Promise((resolve, reject) => {
    const args = ['-i', file, '-af', 'atrim=60:120', 'test/atrim-result-1.mp3', '-af', 'atrim=180:200', 'test/atrim-result-2.mp3', '-y']

    execFile('ffmpeg', args, (error, stdout, stderr) => {
      if (error) reject(error)
      console.log(stdout)
    })
  })
}

module.exports = cut
