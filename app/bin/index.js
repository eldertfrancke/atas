const getUsage = require('command-line-usage')
const commandLineArgs = require('command-line-args')
const trimp3 = require('../lib')

const optionDefinitions = [
  { name: 'file', type: String },
  { name: 'start', type: String },
  { name: 'end', type: String },
  { name: 'split', type: String },
  { name: 'help', type: Boolean }
]

const options = commandLineArgs(optionDefinitions)

const sections = [
  {
    header: 'Make a large sound recording ready for burning to a cd',
    content: 'Remove first and last part of a sound file and optionally split it into two fragments.'
  },
  {
    header: 'Options',
    optionList: [
      {
        name: 'file',
        typeLabel: '[underline]{file}',
        description: 'The input to process.'
      },
      {
        name: 'start',
        typeLabel: '[underline]{timestamp}',
        description: 'Trim time before m:s or h:m:s.'
      },
      {
        name: 'end',
        typeLabel: '[underline]{timestamp}',
        description: 'Trim time after m:s or h:m:s.'
      },
      {
        name: 'split',
        typeLabel: '[underline]{timestamp}',
        description: 'Split in two fragments at m:s or h:m:s.'
      },
      {
        name: 'help',
        description: 'Print this usage guide.'
      }
    ]
  },
  {
    header: 'Examples',
    content: [
      {
        desc: 'With [italic]{start} and [italic]{end} timestap',
        example: '$ trimp3 --file test.mp3 --start 3:45 --end 1:5:33'
      },
      {
        desc: 'Without [italic]{end} timestamp',
        example: '$ trimp3 --file test.mp3 --start 3:45'
      },
      {
        desc: 'Split in to fragements',
        example: '$ trimp3 --file test.mp3 --start 3:45 --end 1:30:35 --split'
      }
    ]
  }
]

const usage = getUsage(sections)

if (!options.file || !options.start || options.help) {
  console.log(usage)
}

if (options.file && options.start) {
  trimp3(options)
    .then(result => console.log('Finished'))
    .catch(err => console.error(err))
}
